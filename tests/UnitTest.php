<?php

namespace App\Post;
use App\Entity\Post;

use PHPUnit\Framework\TestCase;

class UnitTest extends TestCase
{
    public function testPost(): void
    {
        $post = new Post();
        $post->setName("mon premier Post");
        $this->assertTrue($post->getName()==="mon premier Post");
       }
}
